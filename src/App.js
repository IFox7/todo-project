import React from "react";

import { modalOpen } from "./functions/function_btn";
import Btn from "./elements/Button";
import SaveTask from "./elements/SaveTask/SaveTask";
// Головний елемент збірки проекта
function App() {
  return (
    <>
      <div className="container">
        <div className="btnStart_wrapper">
          <Btn className="btn" value="додати ➕" onClick={modalOpen} />
        </div>
        <SaveTask />
      </div>
    </>
  );
}

export default App;
