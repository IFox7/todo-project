import Btn from "../Button";

import "./style.css";
// Елемент виведення завдання покупки(ряд таблиці даних) для відображення на екрані,разом з кнопками
function TextWithButtons({
  task_array,
  fnChangeTask,
  fnDelete,
  fnUnderlineTask,
}) {
  return task_array.map((elem, index) => {
    return (
      <tr key={elem.elementId}>
        <td
          className={elem.hasDone ? "hasDone" : "current_task"}
          id={elem.elementId}
        >
          {elem.element}
        </td>
        <td className="btn_task">
          <Btn value="✍️" onClick={() => fnChangeTask(elem.elementId)} />
        </td>
        <td className="btn_task">
          <Btn value="❌" onClick={() => fnDelete(index)} />
        </td>
        <td className="btn_task underline">
          <Btn value="✅" onClick={() => fnUnderlineTask(elem.elementId)} />
        </td>
      </tr>
    );
  });
}

export default TextWithButtons;
