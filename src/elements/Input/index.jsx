// Елемент інпут модального вікна
function Input({ placeholder, className, value, onChange }) {
  return (
    <>
      <input
        type="text"
        onChange={onChange}
        className={className}
        placeholder={placeholder}
        value={value}
      />
    </>
  );
}

export default Input;
