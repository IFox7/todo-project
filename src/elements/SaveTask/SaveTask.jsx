import React, { Component } from "react";

import ModalWindow from "../ModalWindow";
import TextWithButtons from "../TextWithButtons";
import ModalInputsOnChange from "../ModalInputsOnChange";
import { modalClose2 } from "../../functions/function_btn";

import "./style.css";

export default class SaveTask extends Component {
  // Функція створення випадкового id для елементів
  idMaker = () => {
    return Math.floor(Math.random() * 10000).toString();
  };
  // Об'єкт стейту з велью,масивом елементів і індексом елемента ,що редагується
  state = {
    value: "",
    task_array: [],
    index_change: null,
  };
  // Прописуємо value з інпута модального вікна для відображення в елементі
  changeValue = (e) => {
    this.setState((state) => {
      return {
        ...state,
        value: e.target.value,
      };
    });
  };
  // Виводимо елемент введений через інпут на екран в таблиці
  setValue = () => {
    const wrapper_tasks = document.querySelector(".wrapper_tasks"),
      inp_modal = document.querySelector(".input_modal");
    if (inp_modal.value === "") {
      return;
    } else {
      wrapper_tasks.classList.remove("visibility_tasks");
      //  Об'єкт елемента з випадковим id,текстом введеним в інпут і параметром виконаного завдання в стані фолс
      const newElement = {
        elementId: this.idMaker(),
        element: this.state.value,
        hasDone: false,
      };
      // Вносимо об'єкт елемента в масив стейта
      this.setState((state) => {
        return {
          ...state,
          task_array: [...this.state.task_array, newElement],
        };
      });
    }
    // Очищуємо поле введення даних модального вікна
    if (inp_modal.value !== "") {
      inp_modal.value = "";
    }
  };
  // Функція видалення елемента
  deleteTask = (index) => {
    this.setState((state) => {
      return {
        ...state,
        // task_array:[...this.state.task_array.slice(0, index), ...this.state.task_array.slice(index + 1)],
        task_array: [
          ...this.state.task_array.filter((elem, ind) => ind !== index),
        ],
      };
    });
    if (this.state.task_array.length === 1) {
      const wrapper_tasks = document.querySelector(".wrapper_tasks");
      wrapper_tasks.classList.add("visibility_tasks");
    }
  };
  // Функція перекреслення елементів,щоб позначити "куплені" продукти
  underlineTask = (elementId) => {
    this.state.task_array.map((item) => {
      if (item.elementId === elementId) {
        item.hasDone = !item.hasDone;
      }
      return item;
    });
    this.setState((state) => {
      return {
        ...state,
        task_array: [...this.state.task_array],
      };
    });
  };

  // Блок функцій для редагування обраного елемента
  elementChange = (e) => {
    if (e) {
      this.setState((state) => {
        return {
          ...state,
          value: e.target.value,
        };
      });
    } else return;
  };

  changeTask = (elementId) => {
    const modal_query_wrapper = document.querySelector(".modal2_query_wrapper"),
      inp_modal = document.querySelector(".input_modal2");
    if (modal_query_wrapper.classList.contains("visibility_modal2")) {
      modal_query_wrapper.classList.remove("visibility_modal2");
    }
    const copyArray = Object.assign([], this.state.task_array);
    copyArray.map((elem, index) => {
      if (elem.elementId === elementId) {
        inp_modal.value = elem.element;
        this.setState((state) => {
          return {
            ...state,
            index_change: index,
          };
        });
      }
      return elem;
    });
  };
  // Збереження змін редагованого елемента
  saveChanges = () => {
    const copyArray = Object.assign([], this.state.task_array);
    copyArray.map((elem, index) => {
      if (index === this.state.index_change) {
        this.setState((state) => {
          elem.element = this.state.value;
          return {
            ...state,
            task_array: [
              ...this.state.task_array.slice(0, index),
              elem,
              ...this.state.task_array.slice(index + 1),
            ],
          };
        });
      }
      return elem;
    });
    modalClose2();
  };
  // Рендер елементів на сторінці
  render() {
    return (
      <>
        <ModalWindow onChange={this.changeValue} onClick={this.setValue} />
        <ModalInputsOnChange
          onChange={this.elementChange}
          onClick={this.saveChanges}
        />
        <div className="wrapper_tasks visibility_tasks">
          <table>
            <thead>
              <tr>
                <th className="first_el">Список покупок</th>
                <th>Редагувати</th>
                <th>Видалити</th>
                <th>Куплено</th>
              </tr>
            </thead>
            <tbody>
              <TextWithButtons
                task_array={this.state.task_array}
                fnDelete={this.deleteTask}
                fnUnderlineTask={this.underlineTask}
                fnChangeTask={this.changeTask}
                style={this.styleElement}
                elementId={this.state.elementId}
              />
            </tbody>
          </table>
        </div>
      </>
    );
  }
}
