import { modalClose } from "../../functions/function_btn";

import Btn from "../Button";
import Input from "../Input";

import "./style.css";
// Елемент - модальне вікно
function ModalWindow({ onClick, onChange }) {
  return (
    <>
      <div className="modal_query_wrapper visibility_modal" id="modalWindow">
        <Input
          className="input_modal"
          onChange={onChange}
          placeholder="Додайте покупку"
        />
        <Btn className="btn btn_modal" onClick={onClick} value="Зберегти 📀" />
        <Btn className="btn__modalClose btn" onClick={modalClose} value="❌" />
      </div>
    </>
  );
}

export default ModalWindow;
