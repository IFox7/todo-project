import { modalClose2 } from "../../functions/function_btn";

import Btn from "../Button";
import Input from "../Input";

import "./style.css";
// Модальне вікно редагування елемента
function ModalInputsOnChange({ onClick, onChange }) {
  return (
    <>
      <div className="modal2_query_wrapper visibility_modal2" id="modalWindow2">
        <Input
          className="input_modal2"
          onChange={onChange}
          placeholder="Редагуйте"
        />
        <Btn
          className="btn btn_modal2"
          onClick={onClick}
          value="Зберегти редагування 📀"
        />
        <Btn className="btn__modalClose btn" onClick={modalClose2} value="❌" />
      </div>
    </>
  );
}

export default ModalInputsOnChange;
