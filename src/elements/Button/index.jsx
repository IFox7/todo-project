import "./style.css";
// Елемент кнопок
export default function Btn({ value, className, onClick, id }) {
  return (
    <>
      <button className={className} onClick={onClick} id={id}>
        {value}
      </button>
    </>
  );
}
