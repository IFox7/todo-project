// Функції відкриття-закриття модальних вікон
export function modalOpen() {
  const modalWindow = document.getElementById("modalWindow");
  const input_modal = document.querySelector(".input_modal");
  modalWindow.classList.remove("visibility_modal");
  input_modal.value = "";
}

export function modalClose() {
  const modalWindow = document.getElementById("modalWindow");
  modalWindow.classList.add("visibility_modal");
}

export function modalClose2() {
  const modalWindow = document.getElementById("modalWindow2");
  modalWindow.classList.add("visibility_modal2");
}
